# Imports
import time
import pandas
import csv

# File imports
import config
import numpy

fields_to_add = config.fields_to_add

def open_with_pandas_read_csv(filename, common_field, field_to_copy=False):
    """
    Open csv file to read from format data accordingly
    """
    df = pandas.read_csv(filename, sep=",", header=None, low_memory=False)
    data = df.values
    # formated_data = format_links_csv_data(data, common_field, field_to_copy)
    return data


def read_line(line):
    return [ l.replace('"', '') for l in line.split(",")]

def read_header(line):
    return filter(len, filter(lambda item: (item != ','), line.strip(" ").split('"')))

def format_links_csv_data(data, common_field, field_to_copy, results):
    """
    Format csv data for easier data operations
    """
    headers = [str(x).lower() for x in data[0]]

    results['headers'] = data[0]
    common_field = str(common_field).lower()
    if  common_field in headers:
        common_field_index = headers.index(common_field)
        if field_to_copy:
            field_to_copy = str(field_to_copy).lower()
            for row in data[1:]:
                results[row[common_field_index]] = row[headers.index(field_to_copy)]
        else:
            for row in data[1:]:
                results[row[common_field_index]] = row

    return results


def edit_and_add_headers(headers):
    headers_to_edit = {
        "DealerID" : "user_id",
        "VIN" : "vin",
        "Status" : "status",
        "Year" : "year",
        "Make" : "make_id",
        "Model" : "model_id",
        "Trim" : "trim_id",
        "Mileage" : "mileage",
        "Transmission" : "transmission_id",
        "EngineSize" : "cylinder",
        "Doors" : "doors",
        "OEMColorNameExterior" : "exterior_color_id",
        "OEMColorNameInterior" : "interior_color_id",
        "InternetPrice" : "retail_price",
        "Options" : "addinfo",
    }

    for val in headers:
        if val in headers_to_edit:
            headers[headers.index(val)] = headers_to_edit[val]

    headers = headers[:headers.index("PackageCodes")+1]
    return ['id'] + headers + fields_to_add

def main():

    #  Reading and formatting data

    color_list = open_with_pandas_read_csv(config.reader_dir+"catelog_color.csv", None)
    color_dict = {}

    for c in color_list:
        color_dict[c[2]] = c[0]

    make_list = open_with_pandas_read_csv(config.reader_dir+"catelog_make.csv", None)
    make_dict = {}
    for m in make_list:
        make_dict[m[1]] = m[0]
    model_list = open_with_pandas_read_csv(config.reader_dir+"catelog_model.csv", None)

    model_dict = {}
    model_parent_dict = {}
    for m in model_list:
        model_parent_dict[m[0]] = m[2]
        if m[1] not in model_dict:
            model_dict[m[1]] = [m[0]]
        else:
            model_dict[m[1]].append(m[0])

    trim_list = open_with_pandas_read_csv(config.reader_dir+"catelog_trim.csv", None)

    trim_dict = {}
    trim_parent_dict = {}
    for tr in trim_list:
        trim_parent_dict[tr[0]] = tr[2]
        if tr[1] not in trim_dict:
            trim_dict[tr[1]] = [tr[0]]
        else:
            trim_dict[tr[1]].append(tr[0])

    transmission_list = open_with_pandas_read_csv(config.reader_dir+"catelog_transmission.csv", None)

    transmission_dict = {}
    for t in transmission_list:
        transmission_dict[t[1]] = t[0] 
 
    links_list = open(config.reader_dir+"LINKS.TXT", "r").read().split("\r\n")

    image_dict = {}

    links_headers = read_header(links_list[0])
    links_vin_index = links_headers.index('VIN')
    links_photo_urls_index = links_headers.index('PhotoUrls')
    for links in links_list[1:]:
        link_line = read_line(links)
        try:
            image_dict[link_line[links_vin_index]] = link_line[links_photo_urls_index]
        except IndexError:
            pass

    reference_headers = []
    reference_data = {}

    print "Reading reference file"
    reference_list = open_with_pandas_read_csv(config.reference_file, None)
    reference_headers = reference_list[0].tolist()

    for row in reference_list[1:]:
        reference_data[row[reference_headers.index("vin")]] = row.tolist()
        

    products = open(config.reader_dir+"VEHICLES.TXT", "r").read().split("\r\n")


    # Starting process
    product_list = []
    to_insert = []
    to_update = []
    reference_list = []

    # product headers
    headers = read_header(products[0])
    headers = edit_and_add_headers(headers)
    product_list.append(headers)
    to_insert.append(headers)
    to_update.append(headers)
    reference_list.append(headers)

    
    vin_index = headers.index('vin')
    color_index = headers.index('exterior_color_id')
    make_index = headers.index('make_id')
    model_index = headers.index('model_id')
    transmission_index = headers.index('transmission_id')
    trim_index = headers.index('trim_id')
    package_codes_index = headers.index("PackageCodes")
    status_index = headers.index("status")
    image_index = headers.index('images')
    currency_id_index = headers.index("currency_id")
    post_id_index = headers.index("post_id")
    trash_index = headers.index("trash")
    cylinder_index = headers.index("cylinder")

    # Price & milage index
    retail_price_index = headers.index("retail_price")
    mileage_index = headers.index("mileage")

    reference_data_price_index = reference_headers.index("retail_price")
    reference_data_mileage_index = reference_headers.index("mileage")


    
  
    for idx, p in enumerate(products[1:]):
        color_id = make_id = model_id = trim_id = transmission_id = cylinder_count = ""

        product = read_line(p)
        if len(product) > 2:

            product = [idx+1]+product[:package_codes_index + 1] + [''] * len(fields_to_add)
            vin = product[vin_index]
            color = product[color_index]
            make = product[make_index]
            model = product[model_index]
            transmission = product[transmission_index]
            trim = product[trim_index]
            cylinder = product[cylinder_index]

            
            try:
                color_id = color_dict[color]
            except KeyError, e:
                color_id = ""


            try:
                make_id = make_dict[make]
            except KeyError, e:
                make_id = ""

            try:
                model_id_list = model_dict[model]

                if len(model_id_list) == 1:
                    model_id = model_id_list[0]
                else:
                    for i in model_id_list:
                        if model_parent_dict[i] == make_id:
                            model_id = i
            except KeyError, e:
                model_id = ""

            try:
                trim_id_list = trim_dict[trim]
                if len(trim_id_list) == 1:
                    trim_id = trim_id_list[0]
                else:
                    for i in trim_id_list:
                        if trim_parent_dict[i] == model_id:
                            trim_id = i
            except KeyError, e:
                trim_id = ""
            try:
                transmission_id = transmission_dict[transmission]
            except KeyError, e:
                transmission_id = ""

            try:
                photos = image_dict[vin]
            except KeyError, e:
                photos = ""

            if cylinder:
                cylinder_list = cylinder.split(" ")
                if len(cylinder_list) > 0:
                    try:
                        cylinder_count = int(cylinder_list[0])
                    except ValueError:
                        cylinder_count = ""

            product[color_index] = color_id
            product[make_index] = make_id
            product[model_index] = model_id
            product[transmission_index] = transmission_id
            product[trim_index] = trim_id
            product[image_index] = photos
            product[cylinder_index] = cylinder_count
            
            # Default values
            product[status_index] = 1
            product[currency_id_index] = 2
            product[post_id_index] = 3
            product[trash_index] = 0

            product_list.append(product)

            if vin not in reference_data:
                to_insert.append(product)
                reference_list.append(product)
            else:
                update = False
                

                if product[retail_price_index] != reference_data[vin][reference_data_price_index]:
                    print reference_data[vin][reference_data_price_index]
                    print product[retail_price_index]
                    update = True
                    reference_data[vin][reference_data_price_index] = product[retail_price_index]
                
                if product[mileage_index] != reference_data[vin][reference_data_mileage_index]:
                    update = True
                    print reference_data[vin][reference_data_mileage_index]
                    print product[mileage_index]
                    reference_data[vin][reference_data_mileage_index] = product[mileage_index]

                if update:
                    to_update.append(product)
                reference_list.append(reference_data[vin])


    print "Done with for loop"

    
    if config.write_converted_file:
        print "writing converted file "
        df = pandas.DataFrame(product_list)
        df.to_csv(config.converted_write_file_name, header=False, mode='w', index=False)

    if config.write_insert_file:
        print "writing insert file "
        df = pandas.DataFrame(to_insert)
        df.to_csv(config.to_insert_file_name, header=False, mode='w', index=False)

    if config.write_update_file:
        print "writing updated file "
        df = pandas.DataFrame(to_update)
        df.to_csv(config.to_update_file_name, header=False, mode='w', index=False)


    if (len(to_update) > 1 or len(to_insert) > 1) and config.write_ref_file:
        print "writing reference file"
        df = pandas.DataFrame(reference_list)
        df.to_csv(config.reference_file, header=False, mode='w', index=False)

        

def logError(e):
    """
    Log errors in error dict
    """
    filename = config.LOGS_DIR+'error_{}.log'.format(date.today())
    logf = open(filename, 'w')
    logf.write("{0}\n".format(str(e)))

if __name__ == "__main__":
    # try:
    #     main()
    # except Exception as e:
    #     logError(e)
    main()


import time
import pandas
# import multiprocessing as mp
from collections import OrderedDict
from datetime import date

# File imports
import config



import os, multiprocessing as mp


@profile
# process file function
def processfile(filename, start=0, stop=0):
    print "In process file"
    result = []
    if start == 0 and stop == 0:
        print "process whole file"
    else:
        with open(filename, 'rU') as fh:
            fh.seek(start)
            lines = fh.readlines(stop - start)
            if start == 0 or stop == 0:
                print lines

    return result

@profile
def main():
    # get file size and set chuck size
    filename = config.CSV_READER_DIR + "catelog_product.csv"
    filesize = os.path.getsize(filename)
    split_size = 100*1024*1024

    # determine if it needs to be split
    if filesize > split_size:

        # create pool, initialize chunk start location (cursor)
        pool = mp.Pool(2)
        cursor = 0
        results = []
        with open(filename, 'rU') as fh:

            # for every chunk in the file...
            for chunk in xrange(filesize // split_size):

                # determine where the chunk ends, is it the last one?
                if cursor + split_size > filesize:
                    end = filesize
                else:
                    end = cursor + split_size

                # seek to end of chunk and read next line to ensure you 
                # pass entire lines to the processfile function
                fh.seek(end)
                fh.readline()

                # get current file location
                end = fh.tell()

                # add chunk to process pool, save reference to get results
                proc = pool.apply_async(processfile, args=[filename, cursor, end])
                # print proc.get()

                results.append(proc)

                # setup next chunk
                cursor = end

        # close and wait for pool to finish
        pool.close()
        pool.join()

        # iterate through results

        # for proc in results:
        #     print proc.success()

        # print x[0]

    else:
        print "file size is small"

if __name__ == "__main__":

    main()




# @profile
# def open_with_pandas_read_csv(return_dict, filename):
#     """
#     Open csv file to read from format data accordingly
#     """

#     file = config.CSV_READER_DIR + filename
#     results = []
#     # chunksize = 10 ** 6
#     df = pandas.read_csv(file, sep=config.CSV_DELIMITER, header=None, low_memory=False)
#     results.append(df.values)

#     return results
#     # formated_data = format_links_csv_data(data, common_field, field_to_copy)

#     # dict_var = "from_file" if field_to_copy else "to_file"
#     # return_dict[dict_var] = formated_data






# def main():

#     # for data in config.FILELIST:
#     #     from_file, to_file = start_process(**data)

#     #     headers = to_file["headers"]
#     #     to_copy_index = headers.tolist().index(data['field_to_copy'])
#     #     for key, val in from_file.iteritems():
#     #         if key in to_file and key != "headers":
#     #             if key in to_file:
#     #                 to_file[key][to_copy_index] = val
#     #     write_csv(data, to_file)

    
#     open_with_pandas_read_csv({}, "catelog_product.csv")




# if __name__ == "__main__":
#     main()
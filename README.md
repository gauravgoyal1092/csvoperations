Installation:

- Run `make`


To activate virtualenv:

- Go in csvoperations main folder 
- run `. ./virtualenv/bin/activate`


To run project:

- enable virtualenv
- run python main.py


Forlder Structure:

- `csv` : It contains all txt & csv from sftp (VEHICLES.TXT, trim.csv.. etc)
- `reference_csv`: Database dump table (catelog_product.csv)
- `results`: It contains 3 folders
    - `converted` : Output of viehicles.txt converted with all the data provided
    - `to_write`: data output after comparing converted file and reference csv. (data to insert in database)
    - `to_update`: data output after comparing converted file and reference csv. (data to update in database)
    
    All of the files of result folder are formated like `data-CurrentDate.csv`
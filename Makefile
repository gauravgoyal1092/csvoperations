setup: createVirtualenv install mkdir

live:
	pip install -r requirements.txt mkdir

createVirtualenv:
	virtualenv virtualenv

install:
	. ./virtualenv/bin/activate && pip install -r requirements.txt

mkdir:
	mkdir -p csv logs results reference_csv results/converted results/to_write results/to_update

clean:
	rm -rf virtualenv
	@echo "Cleaned"

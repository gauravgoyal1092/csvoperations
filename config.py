from datetime import datetime

# CONFIGS


CSV_DELIMITER=","

DEBUG=True


reader_dir="csv/"
reference_file_dir="reference_csv/"

csv_writer_dir_converted="results/converted/"
csv_writer_dir_to_update="results/to_update/"
csv_writer_dir_to_insert="results/to_write/"

logs_dir="logs/"


fields_to_add = ['meta_title', 'meta_tag', 'meta_description', 'price_call', 'wsale_price', 'pre_con', 'pre_detail', 'y_from', 'y_to', 'target_price', 'price_to', 'vehicle_runs', 'smoke_smell', 'salvage_title', 'title_status', 'payoff_amount', 'lien_holder', 'number_of_keys', 'manual_present', 'tool_kit', 'vehicle_available', 'last_modified', 'scrap', 'top_color_id', 'currency_id', 'mlg_from', 'mlg_to', 'price_from', 'trash', 'condition_id', 'mileage_type_id', 'video_file_name', 'city_mileage', 'highway_mileage', 'viewed', 'seller_id', 'images', 'post_id', 'car_id', 'product_type_id', 'o_trim', 'subtitle', 'odometer', 'original_miles', 'leased', 'payments_left', 'lien', 'owe', 'clear_title', 'created_on']

write_converted_file = False
converted_write_file_name = csv_writer_dir_converted+"data-"+datetime.now().strftime("%Y%m%d")+".csv"

write_insert_file = True
to_insert_file_name = csv_writer_dir_to_insert+"data-"+datetime.now().strftime("%Y%m%d")+".csv"

write_update_file = True
to_update_file_name = csv_writer_dir_to_update+"data-"+datetime.now().strftime("%Y%m%d")+".csv"

write_ref_file = False
reference_file = reference_file_dir + "catelog_product.csv"


